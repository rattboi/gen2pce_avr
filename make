#!/bin/bash

PROJ=gen2pce

avr-gcc -g -Os -mmcu=atmega328 -DF_CPU=16000000UL -c $PROJ.c
avr-gcc -g -mmcu=atmega328 -o $PROJ.elf $PROJ.o
avr-objcopy -j .text -j .data -O ihex $PROJ.elf $PROJ.hex
sudo avrdude -c usbtiny -p atmega328 -U flash:w:$PROJ.hex
