/*
* genesis to pc-engine converter
* written by: Bradon Kanyid (@rattboi)
* 10/06/2018
*
* ATmega328
*
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define CDELAY (14*20)

#define BUTTON_MODE_3 (0)
#define BUTTON_MODE_6 (1)
#define BUTTON_MODE_3_SWAP (2)

#define RAPID_FIRE_I (0)
#define RAPID_FIRE_II (1)
#define RAPID_FIRE_III (2)

#define RAPID_FIRE_RATE_OFF  (0)
#define RAPID_FIRE_RATE_FAST (_BV(1))
#define RAPID_FIRE_RATE_MED  (_BV(2))
#define RAPID_FIRE_RATE_SLOW (_BV(3))

#define GEN_A     (!!(b0 & 0x10))
#define GEN_START (!!(b0 & 0x20))

#define GEN_UP    (!!(b1 & 0x01))
#define GEN_DOWN  (!!(b1 & 0x02))
#define GEN_LEFT  (!!(b1 & 0x04))
#define GEN_RIGHT (!!(b1 & 0x08))
#define GEN_B     (!!(b1 & 0x10))
#define GEN_C     (!!(b1 & 0x20))

#define GEN_Z     (!!(b3 & 0x01))
#define GEN_Y     (!!(b3 & 0x02))
#define GEN_X     (!!(b3 & 0x04))
#define GEN_MODE  (!!(b3 & 0x08))

#define PCE_I     (0)
#define PCE_II    (1)
#define PCE_SEL   (2)
#define PCE_RUN   (3)

#define PCE_UP    (0)
#define PCE_RIGHT (1)
#define PCE_DOWN  (2)
#define PCE_LEFT  (3)

#define PCE_III   (0)
#define PCE_IV    (1)
#define PCE_V     (2)
#define PCE_VI    (3)

uint8_t gen_banks[4];
uint8_t pce_banks[4];
uint8_t rapid_fire[3];
uint8_t is_controller;
uint8_t is_six_button;
uint8_t num_banks;
uint8_t converter_mode;
volatile uint8_t need_refresh;
volatile uint8_t select;
volatile uint8_t portb;
volatile uint8_t rapid_fire_cnt;

void read_gen_buttons() {
  // first read
  PORTD &= ~(_BV(PORTD7));
  _delay_us(CDELAY);
  PORTD |= _BV(PORTD7);
  _delay_us(CDELAY);

  // second read
  PORTD &= ~(_BV(PORTD7));
  _delay_us(CDELAY);
  gen_banks[0] = PINC & 0x3F;

  PORTD |= _BV(PORTD7);
  _delay_us(CDELAY);
  gen_banks[1] = PINC & 0x3F;

  // third read (6 button?)
  PORTD &= ~(_BV(PORTD7));
  _delay_us(CDELAY);
  gen_banks[2] = PINC & 0x3F;

  PORTD |= _BV(PORTD7);
  _delay_us(CDELAY);
  gen_banks[3] = PINC & 0x3F;

  // enable timer compare interrupt
  //
  // this handles not spinning to wait on the delay
  // between re-reading the genesis controller
  need_refresh = 0;
  TIMSK1 |= _BV(OCIE1A);
}

void check_gen_controller_mode() {
  //                0 1 2 3 4 5
  //genesis bank 0 (U D G G A S)

  is_controller = !(gen_banks[0] & (0x0C)); // controller detect if genesis bank 0 has L+R at the same time (kinda)
  is_six_button = !(gen_banks[2] & (0x0F)); // if genesis bank 2 reads like U+D+L+R at the same time, you have a 6 button controller
}

void set_converter_mode() {
  if (!is_controller) {
    // set to default modes
    converter_mode = BUTTON_MODE_3;
  }

  uint8_t b0 = gen_banks[0];
  uint8_t b1 = gen_banks[1];
  uint8_t b3 = gen_banks[3];

  if (is_six_button) {
    if (!GEN_MODE) {
      if (!GEN_X) {
        if (!GEN_LEFT)
          rapid_fire[RAPID_FIRE_III] = RAPID_FIRE_RATE_SLOW;
        if (!GEN_UP)
          rapid_fire[RAPID_FIRE_III] = RAPID_FIRE_RATE_MED;
        if (!GEN_RIGHT)
          rapid_fire[RAPID_FIRE_III] = RAPID_FIRE_RATE_FAST;
        if (!GEN_DOWN)
          rapid_fire[RAPID_FIRE_III] = RAPID_FIRE_RATE_OFF;
      } else if (!GEN_Y) {
        if (!GEN_LEFT)
          rapid_fire[RAPID_FIRE_II] = RAPID_FIRE_RATE_SLOW;
        if (!GEN_UP)
          rapid_fire[RAPID_FIRE_II] = RAPID_FIRE_RATE_MED;
        if (!GEN_RIGHT)
          rapid_fire[RAPID_FIRE_II] = RAPID_FIRE_RATE_FAST;
        if (!GEN_DOWN)
          rapid_fire[RAPID_FIRE_II] = RAPID_FIRE_RATE_OFF;
      } else if (!GEN_Z) {
        if (!GEN_LEFT)
          rapid_fire[RAPID_FIRE_I] = RAPID_FIRE_RATE_SLOW;
        if (!GEN_UP)
          rapid_fire[RAPID_FIRE_I] = RAPID_FIRE_RATE_MED;
        if (!GEN_RIGHT)
          rapid_fire[RAPID_FIRE_I] = RAPID_FIRE_RATE_FAST;
        if (!GEN_DOWN)
          rapid_fire[RAPID_FIRE_I] = RAPID_FIRE_RATE_OFF;
      } else { // sel pressed, no x/y/z buttons
        if (!GEN_DOWN)
          converter_mode = BUTTON_MODE_3;
        if (!GEN_LEFT)
          converter_mode = BUTTON_MODE_3_SWAP;
        if (!GEN_UP)
          converter_mode = BUTTON_MODE_6;
      }
    }
  } else {
    if (!GEN_START) {
      if (!GEN_DOWN)
        converter_mode = BUTTON_MODE_3;
      if (!GEN_LEFT)
        converter_mode = BUTTON_MODE_3_SWAP;
    }
  }

  num_banks = (converter_mode == BUTTON_MODE_6) ? 4 : 2;
}

void convert_to_pc_engine() {
  if (!is_controller) {
    pce_banks[0] = 0x0F;
    pce_banks[1] = 0x0F;
    pce_banks[2] = 0x0F;
    pce_banks[3] = 0x0F;
    return;
  }

  uint8_t b0 = gen_banks[0];
  uint8_t b1 = gen_banks[1];
  uint8_t b2 = gen_banks[2];
  uint8_t b3 = gen_banks[3];

  uint8_t pce_sel;
  uint8_t pce_run;
  uint8_t gen_a;

  switch (converter_mode) {
    case BUTTON_MODE_6 :
      pce_sel = GEN_MODE;
      pce_run = GEN_START;
      gen_a   = PCE_III;
      break;
    case BUTTON_MODE_3:
      pce_sel = GEN_A;
      pce_run = GEN_START;
      gen_a   = PCE_SEL;
      break;
    case BUTTON_MODE_3_SWAP:
      pce_sel = GEN_START;
      pce_run = GEN_A;
      gen_a   = PCE_RUN;
      break;
    default:
      pce_sel = GEN_A;
      pce_run = GEN_START;
      gen_a   = PCE_SEL;
      break;
  }

  pce_banks[0] = GEN_C     << PCE_I   | // I   -> C
                 GEN_B     << PCE_II  | // II  -> B
                 pce_sel   << PCE_SEL | // (depends on controller mode) -> Start
                 pce_run   << PCE_RUN;  // (depends on controller mode) -> A

  //                0 1 2 3 4 5
  //genesis bank 1 (U D L R B C)
  //pce     bank 1 (U R D L)

  // this block is the same, regardless of 3 button/6 button
  pce_banks[1] = GEN_UP    << PCE_UP    | // Up
                 GEN_RIGHT << PCE_RIGHT | // Right
                 GEN_DOWN  << PCE_DOWN  | // Down
                 GEN_LEFT  << PCE_LEFT;   // Left

  //                0 1 2 3 4 5
  //genesis bank 2 (Z Y X M B C)
  //pce     bank 2 (3 4 5 6)

  pce_banks[2] = GEN_A << PCE_III | // III  -> A
                 GEN_X << PCE_IV  | // VI   -> X
                 GEN_Y << PCE_V   | // V    -> Y
                 GEN_Z << PCE_VI;   // IV   -> Z

  //                0 1 2 3 4 5
  //genesis bank 3 (G G G G A S)
  //pce     bank 3 (G G G G)

  // this bank is always 0, regardless of mode
  // (even though only 6-button mode uses it)
  pce_banks[3] = 0; // All grounded for 6-button

  if (converter_mode != BUTTON_MODE_6) {
    if (rapid_fire[RAPID_FIRE_III]) {
      if ((rapid_fire_cnt & rapid_fire[RAPID_FIRE_III]) && !GEN_X)
        pce_banks[0] &= ~(GEN_A << gen_a);
      else
        pce_banks[0] |= (GEN_A << gen_a);
    }

    if (rapid_fire[RAPID_FIRE_II]) {
      if ((rapid_fire_cnt & rapid_fire[RAPID_FIRE_II])  && !GEN_Y)
        pce_banks[0] &= ~(GEN_B << PCE_II);
      else
        pce_banks[0] |= (GEN_B << PCE_II);
    }

    if (rapid_fire[RAPID_FIRE_I]) {
      if ((rapid_fire_cnt & rapid_fire[RAPID_FIRE_I])   && !GEN_Z)
        pce_banks[0] &= ~(GEN_C << PCE_I);
      else
        pce_banks[0] |= (GEN_C << PCE_I);
    }
  }
}

void init() {
  for (int i = 0; i < 4; i++)
    gen_banks[i] = 0xFF;

  for (int i = 0; i < 3; i++)
    rapid_fire[i] = 0;
  rapid_fire_cnt = 0;

  select = 0;

  need_refresh = 1;
  converter_mode = BUTTON_MODE_3;

  portb = 0xFF;
}

void init_timers() {
  TCCR1B |= 1<<CS11 | 1<<CS10;  //Divide by 64
  OCR1A = 100*16;                  //Count 100 cycles for 6.4 ms interrupt
  TCCR1B |= 1<<WGM12;           //Put Timer/Counter1 in CTC mode
  TIMSK1 |= _BV(OCIE1A);          //enable timer compare interrupt
}

int main(void) {
  init();

  cli();  //Disable global interrupts

  init_timers();

  // Port B outputs to the 74HC157 to the PC-engine
  //   PORTB0-B5 go to the 74HC157
  //   PORTB6/B7 are connected to a crystal/resonator
  //     the last two bits are on PORTD0/D1 instead
  //
  DDRB  = 0xFF;      // set all of port b to outputs
  PORTB = 0xFF;      // start with every output on

  // Port D has the PCE select line interrupt, and the two highest bits of output
  // PD2 (INT0 pin) is the select line input, the rest are outputs (PD0/1,3-7)
  // PD2 has pull-up enabled
  DDRD  = ~(_BV(DDD2));     // Clear the PD2 pins (set as input)
  PORTD = _BV(PORTD2);      // turn On the Pull-ups

  // Port C is for input from Genesis controller
  DDRC  = 0x00;  // Set PortC pins as inputs
  PORTC = 0xFF;  // Enable internal pullups (to determine if controller is plugged in)

  // set INT0 to trigger on high level of PCE select line
  EICRA = (_BV(ISC01) | _BV(ISC00));
  EIMSK |= _BV(INT0);   // Turn on INT0

  sei();  //Enable global interrupts

  while(1) {
    if (need_refresh) {
      read_gen_buttons();
      convert_to_pc_engine();
      check_gen_controller_mode();
      set_converter_mode();
    }
  }
}

// Interrupt routine on rising edge of select line from pc-engine
ISR (INT0_vect) {
  PORTB = portb;
  PORTD &= ~(_BV(PORTD0) | _BV(PORTD1));
  PORTD |= (portb >> 6);
  select = (select + 2) & (num_banks-1);
  portb = ((pce_banks[select] & 0x0F) | ((pce_banks[select+1] & 0x0F) << 4));
  rapid_fire_cnt = ((rapid_fire_cnt + 1) & 0xF);
}

//Interrupt routine when refresh timer completes (for genesis controller reads)
ISR(TIMER1_COMPA_vect) {
  TIMSK1 &= ~(_BV(OCIE1A));   //disable timer compare interrupt
  need_refresh = 1;
}
