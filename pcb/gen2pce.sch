EESchema Schematic File Version 4
LIBS:gen2pce-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Gen2PCE"
Date "2018-10-13"
Rev "1"
Comp "Bradon Kanyid"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328-PU U1
U 1 1 5BBCD8C3
P 3050 3650
F 0 "U1" H 2409 3696 50  0000 R CNN
F 1 "ATmega328-PU" H 2409 3605 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 3050 3650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 3050 3650 50  0001 C CNN
	1    3050 3650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS157 U2
U 1 1 5BBCD9C1
P 5750 1900
F 0 "U2" H 5950 2750 50  0000 C CNN
F 1 "74HC157" H 5950 2650 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5750 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS157" H 5750 1900 50  0001 C CNN
	1    5750 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB9_Male_MountingHoles J3
U 1 1 5BBCDB60
P 7850 3550
F 0 "J3" H 8029 3553 50  0000 L CNN
F 1 "DB9_Male_MountingHoles" H 8029 3462 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mm" H 7850 3550 50  0001 C CNN
F 3 " ~" H 7850 3550 50  0001 C CNN
	1    7850 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic_Shielded:Conn_01x08_Shielded J2
U 1 1 5BBD156A
P 7300 1500
F 0 "J2" H 7387 1415 50  0000 L CNN
F 1 "Conn_01x08_Shielded" H 7387 1324 50  0000 L CNN
F 2 "w_conn_av:minidin-8" H 7300 1500 50  0001 C CNN
F 3 "~" H 7300 1500 50  0001 C CNN
	1    7300 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 1600 6400 1600
Wire Wire Line
	6400 1600 6400 1400
Wire Wire Line
	6400 1400 7100 1400
Wire Wire Line
	6250 1900 6500 1900
Wire Wire Line
	6500 1900 6500 1500
Wire Wire Line
	6500 1500 7100 1500
Wire Wire Line
	6250 2200 6600 2200
Wire Wire Line
	6600 2200 6600 1600
Wire Wire Line
	6600 1600 7100 1600
Text GLabel 7300 2100 3    50   Input ~ 0
GND
Text GLabel 7100 1200 0    50   Input ~ 0
+5V
Wire Wire Line
	7100 1700 6700 1700
Wire Wire Line
	6700 1700 6700 3100
Wire Wire Line
	6700 3100 5150 3100
Wire Wire Line
	5150 3100 5150 2500
Wire Wire Line
	5150 2500 5250 2500
Wire Wire Line
	6800 1800 6800 3000
Wire Wire Line
	5150 3100 5150 4350
Wire Wire Line
	5150 4350 3650 4350
Connection ~ 5150 3100
Wire Wire Line
	5250 1300 4200 1300
Wire Wire Line
	4200 1300 4200 2450
Wire Wire Line
	4200 2450 3650 2450
Wire Wire Line
	5250 1600 4250 1600
Wire Wire Line
	4250 1600 4250 2550
Wire Wire Line
	4250 2550 3650 2550
Wire Wire Line
	5250 1900 4300 1900
Wire Wire Line
	4300 1900 4300 2650
Wire Wire Line
	4300 2650 3650 2650
Wire Wire Line
	5250 2200 4350 2200
Wire Wire Line
	4350 2200 4350 2750
Wire Wire Line
	4350 2750 4050 2750
Wire Wire Line
	5250 1400 4400 1400
Wire Wire Line
	4400 1400 4400 2850
Wire Wire Line
	4400 2850 3750 2850
Wire Wire Line
	4450 1700 4450 2950
Wire Wire Line
	4450 2950 3850 2950
Wire Wire Line
	4450 1700 5250 1700
Wire Wire Line
	5050 2000 5050 4150
Wire Wire Line
	5050 4150 3650 4150
Wire Wire Line
	5050 2000 5250 2000
Wire Wire Line
	5100 2300 5100 4250
Wire Wire Line
	5100 4250 3650 4250
Wire Wire Line
	5100 2300 5250 2300
Wire Wire Line
	3650 4850 6550 4850
Wire Wire Line
	6550 4850 6550 3650
Wire Wire Line
	6550 3650 7550 3650
Text GLabel 7550 3450 0    50   Input ~ 0
GND
Text GLabel 7550 3150 0    50   Input ~ 0
+5V
Text GLabel 7850 4150 3    50   Input ~ 0
GND
Wire Wire Line
	6250 1300 7100 1300
$Comp
L Device:Resonator_Small Y1
U 1 1 5BBE0D29
P 4600 3100
F 0 "Y1" V 4925 3050 50  0000 C CNN
F 1 "Resonator_Small" V 4834 3050 50  0000 C CNN
F 2 "Crystal:Resonator-3Pin_W6.0mm_H3.0mm" H 4575 3100 50  0001 C CNN
F 3 "~" H 4575 3100 50  0001 C CNN
	1    4600 3100
	0    -1   -1   0   
$EndComp
Text GLabel 4800 3100 2    50   Input ~ 0
GND
Wire Wire Line
	3650 3350 5700 3350
Wire Wire Line
	5700 3350 5700 3950
Wire Wire Line
	5700 3950 7550 3950
Wire Wire Line
	3650 3450 5550 3450
Wire Wire Line
	5550 3450 5550 3750
Wire Wire Line
	5550 3750 7550 3750
Wire Wire Line
	3650 3650 5850 3650
Wire Wire Line
	5850 3650 5850 3350
Wire Wire Line
	5850 3350 7550 3350
Wire Wire Line
	3650 3750 5450 3750
Wire Wire Line
	5450 3750 5450 3850
Wire Wire Line
	5450 3850 7550 3850
Wire Wire Line
	5250 3850 5250 3250
Wire Wire Line
	5250 3250 7550 3250
Wire Wire Line
	3650 3850 5250 3850
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5BBF90C9
P 3050 1450
F 0 "J1" H 3100 1767 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 3100 1676 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Vertical" H 3050 1450 50  0001 C CNN
F 3 "~" H 3050 1450 50  0001 C CNN
	1    3050 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3050 4450 3050
Wire Wire Line
	4450 3050 4450 3000
Wire Wire Line
	4450 3000 4500 3000
Wire Wire Line
	3650 3150 4450 3150
Wire Wire Line
	4450 3150 4450 3200
Wire Wire Line
	4450 3200 4500 3200
Wire Wire Line
	3650 3550 7550 3550
Wire Wire Line
	3750 2850 3750 1900
Wire Wire Line
	3750 1900 2650 1900
Wire Wire Line
	2650 1900 2650 1350
Wire Wire Line
	2650 1350 2850 1350
Connection ~ 3750 2850
Wire Wire Line
	3750 2850 3650 2850
Wire Wire Line
	4050 2750 4050 1450
Wire Wire Line
	4050 1450 3350 1450
Connection ~ 4050 2750
Wire Wire Line
	4050 2750 3650 2750
Wire Wire Line
	3850 2950 3850 1800
Wire Wire Line
	3850 1800 2750 1800
Wire Wire Line
	2750 1800 2750 1450
Wire Wire Line
	2750 1450 2850 1450
Connection ~ 3850 2950
Wire Wire Line
	3850 2950 3650 2950
Wire Wire Line
	3650 3950 3950 3950
Wire Wire Line
	3950 3950 3950 1700
Wire Wire Line
	3950 1700 2850 1700
Wire Wire Line
	2850 1700 2850 1550
Text GLabel 3050 5150 3    50   Input ~ 0
GND
Text GLabel 3050 2150 1    50   Input ~ 0
+5V
Text GLabel 3150 2150 1    50   Input ~ 0
+5V
Text GLabel 3350 1550 2    50   Input ~ 0
GND
Text GLabel 3350 1350 2    50   Input ~ 0
+5V
Text GLabel 2450 2450 0    50   Input ~ 0
GND
$Comp
L Device:R_US R1
U 1 1 5BC9AD25
P 4100 3950
F 0 "R1" V 3895 3950 50  0000 C CNN
F 1 "10k" V 3986 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4140 3940 50  0001 C CNN
F 3 "~" H 4100 3950 50  0001 C CNN
	1    4100 3950
	0    1    1    0   
$EndComp
Connection ~ 3950 3950
Text GLabel 4250 3950 2    50   Input ~ 0
+5V
$Comp
L power:+5V #PWR0101
U 1 1 5BCA084A
P 9300 1600
F 0 "#PWR0101" H 9300 1450 50  0001 C CNN
F 1 "+5V" H 9315 1773 50  0000 C CNN
F 2 "" H 9300 1600 50  0001 C CNN
F 3 "" H 9300 1600 50  0001 C CNN
	1    9300 1600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5BCA089C
P 8900 2450
F 0 "#PWR0102" H 8900 2200 50  0001 C CNN
F 1 "GND" H 8905 2277 50  0000 C CNN
F 2 "" H 8900 2450 50  0001 C CNN
F 3 "" H 8900 2450 50  0001 C CNN
	1    8900 2450
	1    0    0    -1  
$EndComp
Text GLabel 7100 1900 0    50   Input ~ 0
GND
Text GLabel 8900 2050 0    50   Input ~ 0
GND
Text GLabel 9200 2050 2    50   Input ~ 0
+5V
$Comp
L Device:C C3
U 1 1 5BCB3824
P 9050 2450
F 0 "C3" V 8798 2450 50  0000 C CNN
F 1 ".1uF" V 8889 2450 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 9088 2300 50  0001 C CNN
F 3 "~" H 9050 2450 50  0001 C CNN
	1    9050 2450
	0    1    1    0   
$EndComp
Connection ~ 8900 2450
$Comp
L Device:CP1 C1
U 1 1 5BCB392F
P 9050 1600
F 0 "C1" V 8708 1600 50  0000 C CNN
F 1 "10uF" V 8799 1600 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" V 8890 1600 50  0000 C CNN
F 3 "~" H 9050 1600 50  0001 C CNN
	1    9050 1600
	0    1    1    0   
$EndComp
Connection ~ 9200 1600
Wire Wire Line
	9200 1600 9200 2050
Wire Wire Line
	8900 1600 8900 2050
$Comp
L Device:C C2
U 1 1 5BCBBAFB
P 9050 2050
F 0 "C2" V 8708 2050 50  0000 C CNN
F 1 ".1uF" V 8799 2050 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" V 8890 2050 50  0000 C CNN
F 3 "~" H 9050 2050 50  0001 C CNN
	1    9050 2050
	0    1    1    0   
$EndComp
Connection ~ 9200 2050
Wire Wire Line
	9200 2050 9200 2450
Connection ~ 8900 2050
Wire Wire Line
	8900 2050 8900 2450
Wire Wire Line
	6800 1800 7100 1800
Wire Wire Line
	5250 2600 5200 2600
Wire Wire Line
	5200 2600 5200 3000
Wire Wire Line
	5200 3000 6800 3000
NoConn ~ 3650 4450
NoConn ~ 3650 4550
NoConn ~ 3650 4650
NoConn ~ 3650 4750
$Comp
L power:+5V #PWR0103
U 1 1 5BC04DF7
P 5750 1000
F 0 "#PWR0103" H 5750 850 50  0001 C CNN
F 1 "+5V" H 5765 1173 50  0000 C CNN
F 2 "" H 5750 1000 50  0001 C CNN
F 3 "" H 5750 1000 50  0001 C CNN
	1    5750 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 1600 9300 1600
$Comp
L power:GND #PWR0104
U 1 1 5BC0D56D
P 5750 2900
F 0 "#PWR0104" H 5750 2650 50  0001 C CNN
F 1 "GND" H 5755 2727 50  0000 C CNN
F 2 "" H 5750 2900 50  0001 C CNN
F 3 "" H 5750 2900 50  0001 C CNN
	1    5750 2900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
